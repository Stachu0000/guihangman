import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.concurrent.CountDownLatch;

public class HangmanFrame {
    private final JFrame frame = new JFrame("Gra Wisielec :)");
    private final JPanel panelSouth = new JPanel();
    private final JPanel panelNorth = new JPanel();
    private final JPanel panelWest = new JPanel();
    private final JTextArea textAreaNorth = new JTextArea();
    private final JTextArea textAreaSouth = new JTextArea();
    private final JTextField textField = new JTextField(5);
    private final JLabel label = new JLabel("Wpisz tutaj");
    private final JButton button = new JButton("Wyślij");
    private String input;
    private ImagePanel imagePanel;
    private CountDownLatch latch = new CountDownLatch(1);

    public HangmanFrame() {
        initComponents();
    }

    public void setTextAreaNorth(String text) {
        this.textAreaNorth.setText(text);
    }

    public void setTextAreaSouth(String text) {
        this.textAreaSouth.setText(text);
    }

    public String getInput() throws InterruptedException {
        latch.await();
        return input;
    }

    private void initComponents() {
        Image startImage = new Images().getStartImage();

        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        setPanelNorth();

        setPanelSouth();

        setPanelWest();

        setPanelCenter(startImage);

        buttonAction();

        frame.setSize(1200, 800);
        frame.setVisible(true);

    }

    class ButtonListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            input = textField.getText();
            textField.setText("");
            latch.countDown();
        }
    }

    private void buttonAction() {
        button.addActionListener(new ButtonListener());
    }

    private void setPanelWest() {
        textField.setFont(new Font("Arial", Font.PLAIN, 30));
        panelWest.add(label);
        panelWest.add(textField);
        panelWest.add(button);

        frame.getContentPane().add(BorderLayout.WEST, panelWest);
    }

    private void setPanelNorth() {
        textAreaNorth.setFont(new Font("Arial", Font.PLAIN, 40));

        panelNorth.add(textAreaNorth);
        frame.getContentPane().add(BorderLayout.NORTH, panelNorth);
    }

    private void setPanelSouth() {
        textAreaSouth.setFont(new Font("Arial", Font.PLAIN, 40));

        panelSouth.add(textAreaSouth);
        frame.getContentPane().add(BorderLayout.SOUTH, panelSouth);
    }

    public void setPanelCenter(Image image) {

        imagePanel = new ImagePanel(image);
        frame.getContentPane().add(BorderLayout.CENTER, imagePanel);
    }

    public void changeImage(Image image) {
        imagePanel.changeImage(image);
        frame.repaint();
    }

    public void resetLatch() {
        latch = new CountDownLatch(1);
    }


}