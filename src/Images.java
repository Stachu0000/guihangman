import java.util.ArrayList;
import javax.swing.ImageIcon;
import java.awt.*;

public class Images {

    private ArrayList<Image> images = new ArrayList<Image>();
    Image image1 = new ImageIcon(getClass().getResource("/images/wisielec1.png")).getImage();
    Image image2 = new ImageIcon(getClass().getResource("/images/wisielec2.png")).getImage();
    Image image3 = new ImageIcon(getClass().getResource("/images/wisielec3.png")).getImage();
    Image image4 = new ImageIcon(getClass().getResource("/images/wisielec4.png")).getImage();
    Image image5 = new ImageIcon(getClass().getResource("/images/wisielec5.png")).getImage();
    Image image6 = new ImageIcon(getClass().getResource("/images/wisielec6.png")).getImage();
    Image image7 = new ImageIcon(getClass().getResource("/images/wisielec.png")).getImage();
    Image happyImage = new ImageIcon(getClass().getResource("/images/happy.png")).getImage();
    Image startImage = new ImageIcon(getClass().getResource("/images/startImage.png")).getImage();

    public Images() {
        this.images.add(image1);
        this.images.add(image2);
        this.images.add(image3);
        this.images.add(image4);
        this.images.add(image5);
        this.images.add(image6);
        this.images.add(image7);
    }

    public ArrayList<Image> getImages() {
        return images;
    }

    public Image getStartImage(){
        return startImage;
    }

    public Image getHappyImage() {
        return happyImage;
    }
}
