import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class Game {
    Images images = new Images();
    ArrayList<Image> imagesList = images.getImages();
    Image happyImage = images.getHappyImage();
    String password;
    String emptyPassword;
    HangmanFrame hFrame = new HangmanFrame();
    int counter = 0;

    public void Start() {
        hFrame.setTextAreaNorth("Witaj. Podaj hasło do gry.");
        setUpGamePassword();

        hFrame.setTextAreaNorth("Podaj literę do hasła.");

        while (true) {
            String letter = getInput();

            if (!password.contains(letter)) {

                if (lastHangman()) {
                    break;
                }
                hFrame.setTextAreaNorth("Pudło!!! Spróbuj raz jeszcze.");
                hFrame.changeImage(imagesList.get(counter));

                counter++;

            } else {

                int index = password.indexOf(letter);
                StringBuilder sb = new StringBuilder(emptyPassword);

                while (index >= 0) {
                    sb.setCharAt(index, letter.charAt(0));
                    index = password.indexOf(letter, index + 1);
                }
                emptyPassword = sb.toString();

                if (!emptyPassword.contains("_")) {
                    hFrame.setTextAreaNorth("Wygrałeś, uzupełniłeś hasło: " + password);
                    hFrame.setTextAreaSouth(password);
                    hFrame.changeImage(happyImage);
                    break;
                }

                hFrame.setTextAreaNorth("Brawo, trafiłeś.\r\nCzy chcesz spróbować odgadnąć pełne hasło? Wpisz: tak lub nie.");
                hFrame.setTextAreaSouth(emptyPassword);
                var guess = getInput();
                String answer = validateAnswer(guess);

                if (answer.equalsIgnoreCase("tak")) {
                    hFrame.setTextAreaNorth("Podaj hasło");

                    if (!getInput().equals(password)) {
                        if (lastHangman()) {
                            break;
                        }
                        hFrame.setTextAreaNorth("Pudło, gramy dalej.");
                        hFrame.changeImage(imagesList.get(counter));
                        counter++;

                    } else {
                        hFrame.setTextAreaNorth("Brawo, podałeś prawidłowe hasło: " + password.toUpperCase());
                        hFrame.setTextAreaSouth(password);
                        hFrame.changeImage(happyImage);
                        break;
                    }
                }
                else{
                    hFrame.setTextAreaNorth("Podaj literę do hasła.");
                }
            }
        }
    }

    private boolean lastHangman() {
        if (counter == imagesList.size() - 1) {
            hFrame.setTextAreaNorth("Przegrałeś!!!!!!!!!!!!!");
            hFrame.changeImage(imagesList.get(counter));
            return true;
        }
        return false;
    }

    private String validateAnswer(String guess) {
        var answer = guess;

        while (!answer.equalsIgnoreCase("tak") && !answer.equalsIgnoreCase("nie")) {
            hFrame.setTextAreaNorth("Wpisz tak lub nie");
            answer = getInput();
        }
        return answer;
    }

    private void setUpGamePassword() {

        password = getInput();
        emptyPassword = "_".repeat(password.length());
        hFrame.setTextAreaSouth(emptyPassword);
    }

    private String getInput() {
        String result = "";

        try {
            hFrame.resetLatch();
            result = hFrame.getInput();
        } catch (InterruptedException e) {
            e.getStackTrace();
        }
        return result;
    }
}
